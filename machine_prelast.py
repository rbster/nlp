import os
import Stemmer
import re
import argparse

def get_dict(path, part):  # Считываем название файлов книг(имен книг.txt)
    result = dict()
    dirs = os.listdir(path)
    for dir in dirs:
       files = os.listdir(path +"/" + dir)
       train_size = int(len(files) * part / 100)  # первую часть "part" файлов берем для обучения
       train = files[0:train_size]
       test = files[train_size:len(files)]
       result[dir] = (train, test)
    return result


def get_stop():  # извлечем стоплист из текстового документа в корневой папке программы, там слова через пробел
    f = open('stop_list.txt')
    stop_list = []
    for line in f:
        line = line.split()
        for word in line:
           stop_list.append(word)
    f.close()
    return stop_list


def texter(path_to_lib, author, doc):  # Непосредственно считываем данные текста
# органезуем стоп-лист из местоимений, которых много в любом тексте(их совпадения не важны)
    stop_list = get_stop()
    bigramms = {}
    unigramms = {}
    trigramms = {}
    n_words = 0
    n_biwords = 0
    n_triwords = 0
    t = open(path_to_lib + '/' + author + '/' + doc)
    stemmer = Stemmer.Stemmer('russian')  # Обозначим стеммер, настроенный на русский язык
    last_word = ''  # Рассматриваем последннее слово если оно не кончается знаком препинания, чтобы если оно не нулевое добавить его к началу строки.
    pre_last = ''  # рассматриваем предпоследнее слово, если в нем и в последнем нет точки на конце, тогда добавим его в начало предыдущей строки, перед последующим для учета триграмм
    for line in t:
        line = line.split(' ')
        delet = []
        for i in range(len(line)):
            if re.search(r'[^А-Яа-я.,!&:;\n]', line[i]):  # пометим слова содержащие посторонние символы, слова не русского языка. исключим слова с дефисом, они редки...
               delet.append(i)  # ...и часто являются местоимениями, которые мы не хотим рассматривать(они излишне распространены)
        for num in delet:  # заменим их разделительной точкой, которую в дальнейшем поставим в конец слова. Избавимся от некорректных слов, не будем рассматривать как биграмму два соседних.
            line[num] = '.'  # предположим, что знаки препинания расставленны корректно(что не всегда так), или некорректных пренебрежимо мало, тогда знак препинания включен в наше рассматриваемое слово,
                        # стоит на последнем месте. Проверим, что  они не встречаются в середине, написанные неправильно исключим, так как они все же могут попадаться, но их мало
        delet = []
        for i in range(len(line)):
            if (re.match(r'[А-Яа-я]*[^А-Яа-я]+[А-Яа-я]+', line[i])):  # Находим слова(состоящие только из букв и знаков препинания), содержащие некорректные знаки препинания
                delet.append(i)  # знаки пунктуации в середине или начале
        for num in delet:  # заменяем точками, чтобы присчитывании биграмм их учесть
            line[num] = '.'
        for i in range(len(line) - 1):  # допишем точку в конец слов, после которых стоит слово == '.'
            if line[i + 1] == '.':
                line[i] += '.'          # таким образом допускаем знаки препинания только в конце слов

        new_line = []
        for i in range(len(line)):
            if not (len(line[i]) <= 3 or (len(line[i]) > 3 and re.match(r'^[А-Яа-я]{,3}[^А-Яа-я]+', line[i]))):  # исключаем строки, за не более чем 3мя буквами идут знаки препинания
                stop = False
                for word in stop_list:  # исключаем слова из стоп листа
                   if re.match(word + r'\M*', line[i]):
                      stop = True
                      break
                if not stop:
                    new_line.append(line[i])  # получили набор слов со знаками препинания на конце. Теперь считаем из строки юни- и биграммы
        line = new_line

        for i in range(len(line)):  # Для начала заменим все знаки препинания на одну точку в конце каждого слова для удобства
            if re.match(r'^[А-Яа-я]+[;:,.!?]+\n?', line[i]):  # сразу воспользуемся стеммером для преобразования наших слов к форме похожей на нормальную
                line[i] = stemmer.stemWord(re.match(r'^[А-Яа-я]+', line[i]).group(0)).lower() + '.'  # Для этого используем библиотеку PyStemmer - обертку для Snowball stemmer
            elif re.match(r'^[А-Яа-я]+[\n]?', line[i]):
                line[i] = stemmer.stemWord(re.match(r'^[А-Яа-я]+', line[i]).group(0)).lower()  # Включен случай, где на конце слова был перенос строки!

        # разбили каждую строчку текста, теперь будем считать количества повторений каждых слов, затем вероятности
        for word in line:  # юниграммы и общее число слов, чтобы посчитать вероятность каждой
           fix_word = word
           if word[len(word) - 1] == '.':
              fix_word = word[:-1]
           unigramms[fix_word] = unigramms.get(fix_word, 0) + 1
           n_words += 1

        if len(last_word) > 0:  # учитываем последнее слово предыдущей строки, если оно не кончается на точку(учет этого в следующем цикле)
           line.insert(0, last_word)
        # будем сохранять последнее слово строки, если оно не кончается точкой - это будет значить, что это слово и слово следующей строки образуют биграмму
        # Чтобы учесть это, добавим его в следующую строку, если настоящая строка - не последняя
        if len(line) > 0:
            last_word_index = len(line) - 1
            if (line[last_word_index][len(line[last_word_index]) - 1] == '.'):  # Рассмотрим последний символ последнего слова
                last_word = ''
            else:
                last_word = line[len(line) - 1]  # ОБРАТИТЬ ВНИМАНИЕ! слова добавляются в строчки после подсчета слов, но до подсчета общего числа слов и юниграмм

        for i in range(len(line) - 1):  # считаем биграммы
            if line[i][len(line[i]) - 1] != '.':
                n_biwords += 1  # посчитаем общее кол-во биграмм
                word_2 = line[i + 1]
                if line[i + 1][len(line[i + 1]) - 1] == '.':
                    word_2 = line[i + 1][:-1]
                bigramms[line[i] + ' ' + word_2] = bigramms.get(line[i] + ' ' + word_2, 0) + 1  # первое слово ' ' второе слово(встречаем второе слово, при условии первого)
       # после подсчета биграмм добавим пред последнее слово предыдущей строки, если оно есть и не кончается на . и последнее не кончается на точку
        if len(pre_last) > 0:
            line.insert(0, pre_last)
        if len(line) > 1:
            pre_last_index = len(line) - 2
            if line[pre_last_index][len(line[pre_last_index]) - 1] == '.' or last_word == '':
                pre_last = ''
            else:
                pre_last = line[pre_last_index]  # добовляем предпоследнее слово в следующую строку, если нет точек в последних двух
        # теперь посчитаем триграммы.
        for i in range(len(line) - 2):
            if line[i][len(line[i])-1] != '.' and line[i + 1][len(line[i + 1]) -1] != '.':
                n_triwords += 1  # посчитаем общее число триграмм
                word_3 = line[i + 2]
                if word_3[len(word_3) - 1] == '.':
                    word_3 = word_3[:-1]
                trigramms[line[i] + ' ' + line[i + 1] + '|' + word_3] = trigramms.get(line[i] + ' ' + line[i + 1] + '|' + word_3, 0) + 1
      # Посчитали количество встречающихчя трех слов подряд 
   # Имеем хеш таблица с кол-вами юни- и биграмм
   # Преобразуем их к хеш-таблицам вероятностей. Каждую юниграмму доделим на число слов.
    unigramms_prob = {}
    for item in unigramms.items():  # рассчитаем вероятность юниграмм
       unigramms_prob[item[0]] = item[1] / n_words  # вероятность встретить слово в тексте 
   
    bigramms_prob = {}  # найдем вероятности биграмм!
    for item in bigramms.items():
        key = item[0].split()
        value = item[1]
##        value /= unigramms[key[0]]  # Посчитаем условную вероятность второго слова при условии первого.  ## Посчитаем доделив количество  конкретных биграмм на их обшее кол-во

        bigramms_prob[key[0] + ' ' + key[1]] = value / n_biwords  ##value * unigramms_prob[key[0]]  # (Домножим на вероятность первого, получим вероятность того, что слова идут подряд)

    trigramms_prob = {}  # найдем вероятности триграмм
    for item in trigramms.items():
        key = item[0].split('|')
        value = item[1]
##        value /= bigramms[key[0]]  # Условная вероятность встретить третье слово, после первых двух . Первое слово точно было учтено, делим не на ноль
        # посчитаем скорректированную на g условную вероятность. домножим ее на g и прибавим вероятность вероятность последнего слова*(1-g)
##        a = 0.4  # 0.4
##        b = 0.4  # 0.4 лучшие результаты
##        value = (1 - (a + b)) * value + a * bigramms_prob[key[0]] + b * unigramms_prob[key[1]]  # МЕТОД ИНТЕРПОЛЯЦИИ ДЛЯ ТРИГРАММ
        trigramms_prob[key[0] + ' ' + key[1]] = value / n_triwords ## value * bigramms_prob[key[0]]  # Аналогично биграммам, считаем вероятность триграмм  ## ПОСЧИТАЕМ ПРОСТО

    t.close()
    return (unigramms_prob, bigramms_prob, trigramms_prob)

         
def coss(train, test):  # подаем на вход кортеж из имени автора, хеш таблиц вероятностей юни и биграмм, возвращаем пару - косинусы между юниграммами и биграммами
    u_abs_x = 0  # модуль тестового вектора юниграмм
    b_abs_x = 0  # модуль тестового вектора биграмм

    t_abs_x = 0
    
    for elem in train[1].values():  # сосчитаем сумму квадратов координат
        u_abs_x += elem**2
    u_abs_x = u_abs_x**0.5  # возьмем из нее корень
    for elem in train[2].values():  # аналогично для биграмм
        b_abs_x += elem**2
    b_abs_x = b_abs_x**0.5
    
    for elem in train[3].values():  # аналогично для триграмм
        t_abs_x += elem**2
    t_abs_x = t_abs_x**0.5
    
    
    u_abs_y = 0  # аналогично для тестовых
    b_abs_y = 0
    
    t_abs_y = 0
    
    for elem in test[1].values():
        u_abs_y += elem**2
    u_abs_y = u_abs_y**0.5
    for elem in test[2].values():  # аналогично для биграмм
        b_abs_y += elem**2
    b_abs_y = b_abs_y**0.5

    for elem in test[3].values():  # аналогично для триграмм
        t_abs_y += elem**2
    t_abs_y = t_abs_y**0.5

    # теперь посчитаем скалярное произведение юниграмм. для начала перепишем все ключи юниграм в множество
    keys = []
    for key in train[1].keys():
        keys.append(key)
    for key in test[1].keys():
        keys.append(key)
    keys = set(keys)   # избавимся от повторов
    keys = list(keys)
    u_product = 0
    for key in keys:
        u_product += train[1].get(key, 0) * test[1].get(key, 0)  # посчитаем скалярное произведение для юниграмм

    # теперь аналогично посчитаем скалярное произведение биграмм
    keys = []
    for key in train[2].keys():
        keys.append(key)
    for key in test[2].keys():
        keys.append(key)
    keys = set(keys)   # избавимся от повторов
    keys = list(keys)
    b_product = 0
    for key in keys:
        b_product += train[2].get(key, 0) * test[2].get(key, 0)  # посчитаем скалярное произведение для юниграмм

    keys = []  # аналогично для произведения векторов вероятностей триграмм
    for key in train[3].keys():
        keys.append(key)
    for key in test[3].keys():
        keys.append(key)
    keys = set(keys)   # избавимся от повторов
    keys = list(keys)
    t_product = 0
    for key in keys:
        t_product += train[3].get(key, 0) * test[3].get(key, 0)  # посчитаем скалярное произведение для юниграмм

    return (u_product / (u_abs_x * u_abs_y), b_product / (b_abs_x * b_abs_y), t_product / (t_abs_x * t_abs_y))  # возвращаем тройку косинусов


def cos_method(train, item):
    final_author = ''
    max_cos = [-2, -2, -2]  # максимальная сумма квадратов косинусов
    max_author = ['', '', '']  # будем определять максимально подходящего автора(т.е. того у кого бОльший корень суммы квадратов косинусов)
        
    for instance in train:
        u, b, t = coss(instance, item)  # косинусы юни-, би- и триграмм 
        if max_cos[0] < u:            # пересечем все найденные решения, для увеличения достоверности
            max_cos[0] = u
            max_author[0] = instance[0]
        if max_cos[1] < b:   
            max_cos[1] = b
            max_author[1] = instance[0]
        if max_cos[2] < t:   
            max_cos[2] = t
            max_author[2] = instance[0]
    if max_author[0] == max_author[1] and max_author[1] == max_author[2]:
        final_author = max_author[0]
    elif max_author[0] == max_author[1]:
        final_author = max_author[0]
    elif max_author[1] == max_author[2]:
        final_author = max_author[1]
    else:
        Max_cos = -2
        for i in range(3):
            if Max_cos < max_cos[i]:
                Max_cos = max_cos[i]
                final_author = max_author[i]
    return final_author


def evk_dist(instance, item, p):  # Считаем расстояния по метрикам зависящим от p,  p =2 - евклидово расстояние, >p минковского
    u_d = 0
    b_d = 0
    t_d = 0

    keys_1 = []
    for key in instance[2].keys():
        keys_1.append(key)
    keys_2 = []
    for key in item[2].keys():
        keys_2.append(key)
    keys = []
    keys_1 = set(keys_1)
    keys_2 = set(keys_2)
    keys = keys_1.intersection(keys_2)
    keys = list(keys)
    for key in keys:
        b_d = abs(instance[2].get(key, 0) - item[2].get(key, 0)) ** p
    b_d = b_d ** (1/p)
    return b_d  # для начала испробуем на юниграммах


def neib_method(train, item, k, p):  # от p зависит метрика
    ranges = []  # расстояния от тренировочных до тестового
    authors = []  # авторы соответственно

    for instance in train:
        dist = evk_dist(instance, item, p)
        ranges.append((dist, instance[0]))

    ranges.sort(key = lambda x: x[0])  # сортируем по расстоянию и обрасываем все кроме k
    min_ranges = ranges[:k]

    for i in range(k):
        authors.append(min_ranges[i][1])
    pred = True
    while pred:  # пока не найдется автора, который написал имеет больше всех текстов среди k, или останется ближайший, будем отбрасывать последних
        dic = {}
        for word in authors:
            dic[word] = dic.get(word, 0) + 1
        m = 0  # найдем автора у которого больше всех текстов среди k
        m_auth = '' # максимальный автор
        for elem in dic.items():
            if m < elem[1]:
                m = elem[1]
                m_auth = elem[0]

        for elem in dic.items():
            if (m > elem[1] and m_auth != elem[0]) or (m == elem[1] and m_auth == elem[0]) or len(authors) == 1:
                pred = False
            else:
                pred = True
                authors.pop()
                break

    return m_auth
        
def algo_efficiency(books, path, out_way):  # считаем вероятности для юни- и биграмм каждого текста, используя функцию texter, что выше
    train = []  # список кортежей, в каждом из которых - Автор, хеш вероятностей юниграм, хеш вероятностей биграм)
    test = []  # список аналогичных кортежей, на которых будем проверять алгоритм
    out = open(out_way, 'w')
    for author_text_train_or_test in books.items():  # рассмотрим пары автор - пара его тексты для обучения, его тексты для теста алгоритма ### РАБОТАЕТ 4 МИНУТЫ!!!
        for text in author_text_train_or_test[1][0]:
            u_p_train, b_p_train, t_p_train = texter(path, author_text_train_or_test[0], text)  # 2 вектора вероятностей юниграмм и биграмм, триграмм в паре для каждого тестового текста + триграмм теста
            train.append((author_text_train_or_test[0], u_p_train, b_p_train, t_p_train))
            
        for text in author_text_train_or_test[1][1]:
            u_p_test, b_p_test, t_p_test = texter(path, author_text_train_or_test[0], text)  # 2 вектора вероятностей юниграмм и биграмм, триграмм на которых будем тестировать
            test.append((author_text_train_or_test[0], u_p_test, b_p_test, t_p_test))
    texts_n = 0  # учтем общее число авторов
    win_n = 0  # будем учитывать количество правильно угаданных авторов
    for item in test:  # для каждого кортежа теста считаем максимальную сумму косинусов юни и биграмм
        texts_n += 1
        final_author = ''
        final_author = cos_method(train, item)  # метод косинусов
        if final_author == item[0]:  # значит алгоритм нашел правильного автора
            win_n += 1
            out.write('+')
        out.write(item[0] + '->' + final_author + '\n')
    result = win_n / texts_n * 100
    out.write("efficiecy of algorithm is equal " + str(result) + " %\n")
    return result


def Main():  # Простеий ввод данных с помощью консоли и вызов основных функций
    parser = argparse.ArgumentParser(description = "Find texts' authors.\n Algo shows their efficiency")  # Используем библиотеку Argparse, для считывания данных консоли 
    parser.add_argument("path",
                        help = "write the path to folder with folders called name of author with texts")
    parser.add_argument("--part", type = int,
                        help = "write the persentage of training texts among test texts",
                        default = 33)
    parser.add_argument("--out",
                        help = "write the path to out.txt file",
                        default = "out.txt")

    args = parser.parse_args()  # там хранятся значения введенных параметров
    
    books = get_dict(args.path, args.part)
    algo_efficiency(books, args.path, args.out)
    print("Algorithm has finished. You can find the result in out.txt or in *.txt given to argument 'out'")

if __name__ == "__main__":  # Если запускаем этот файл, то вызывается функция Main
    Main()





